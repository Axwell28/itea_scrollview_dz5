//
//  ViewController.swift
//  ITEA_ScrollView
//
//  Created by Alex Marfutin on 7/4/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet var nameText: UITextField!
    @IBOutlet var surnameText: UITextField!
    @IBOutlet var ageText: UITextField!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var phoneText: UITextField!
    @IBOutlet var cityText: UITextField!
    
    @IBOutlet var nameUnderlineLabel: UILabel!
    @IBOutlet var surnameUnderlineLabel: UILabel!
    @IBOutlet var ageUnderlineLabel: UILabel!
    @IBOutlet var emailUnderlineLabel: UILabel!
    @IBOutlet var phoneUnderlineLabel: UILabel!
    @IBOutlet var cityUnderlineLabel: UILabel!
    
    @IBOutlet var nameErrorLabel: UILabel!
    @IBOutlet var surnameErrorLabel: UILabel!
    @IBOutlet var ageErrorLabel: UILabel!
    @IBOutlet var emailErrorLabel: UILabel!
    @IBOutlet var phoneErrorLabel: UILabel!
    @IBOutlet var cityErrorLabel: UILabel!
    
    @IBOutlet var nameErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet var surnameErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet var ageErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet var emailErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet var phoneErrorLabelHeight: NSLayoutConstraint!
    @IBOutlet var cityErrorLabelHeight: NSLayoutConstraint!
    
    var nameEx = ""
    var surnameEx = ""
    var ageEx = ""
    var phoneEx = ""
    var emailEx = ""
    var cityEx = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameText.delegate = self
        surnameText.delegate = self
        ageText.delegate = self
        phoneText.delegate = self
        emailText.delegate = self
        cityText.delegate = self
        self.hideKeyboardWhenTappedAround()
    }

    func checkNameSurnameText() -> Bool {
        if let name = nameText.text , let surname = surnameText.text {
            if !name.isEmpty {
                if !surname.isEmpty {
                    if name.count > 2 && name.count < 60 {
                        nameEx = name
                        if surname.count > 2 && surname.count < 60 {
                            surnameEx = surname
                            return true
                        } else {
                            setErrorLabelConfiguration(underlineLabel: surnameUnderlineLabel, errorLabel: surnameErrorLabel, constrain: surnameErrorLabelHeight, text: "Неправильное количество символов! Поле должно содержать от 2 до 60 символов")
                            return false
                        }
                    } else {
                    setErrorLabelConfiguration(underlineLabel: nameUnderlineLabel, errorLabel: nameErrorLabel, constrain: nameErrorLabelHeight, text: "Неправильное количество символов! Поле должно содержать от 2 до 60 символов")
                    return false
                    }
                } else {
                    setErrorLabelConfiguration(underlineLabel: surnameUnderlineLabel, errorLabel: surnameErrorLabel, constrain: surnameErrorLabelHeight, text: "Ошибка! Поле пустое. Заполните поле обязательно!")
                    return false
                }
            } else {
            setErrorLabelConfiguration(underlineLabel: nameUnderlineLabel, errorLabel: nameErrorLabel, constrain: nameErrorLabelHeight, text: "Ошибка! Поле пустое. Заполните поле обязательно!")
            return false
            }
        } else {
            return false
        }
    }
    
    func checkAgeText() -> Bool {
        if let age = ageText.text {
            if !age.isEmpty {
                if let ageIn = Int(age), ageIn < 150 , ageIn > 1 {
                    ageEx = age
                    return true
                } else {
                    setErrorLabelConfiguration(underlineLabel: ageUnderlineLabel, errorLabel: ageErrorLabel, constrain: ageErrorLabelHeight, text: "Неправильно указан возраст! Поле должно быть больше 1 и меньше 150 лет!")
                    return false
                }
            } else {
                setErrorLabelConfiguration(underlineLabel: ageUnderlineLabel, errorLabel: ageErrorLabel, constrain: ageErrorLabelHeight, text: "Ошибка! Поле пустое. Заполните поле обязательно!")
                return false
            }
        } else {
        return false
        }
    }
    
    func checkPhoneText() -> Bool {
        if let phone = phoneText.text {
            if !phone.isEmpty {
                if phone.contains("+") {
                    if phone.count > 3, phone.count < 14 {
                        phoneEx = phone
                        return true
                    } else {
                        setErrorLabelConfiguration(underlineLabel: phoneUnderlineLabel, errorLabel: phoneErrorLabel, constrain: phoneErrorLabelHeight, text: "Ошибка! Неправильный номер, поле должно содержать от 3 до 14 символов")
                        return false
                    }
                } else {
                    setErrorLabelConfiguration(underlineLabel: phoneUnderlineLabel, errorLabel: phoneErrorLabel, constrain: phoneErrorLabelHeight, text: "Ошибка! Неправильно введен номер телефона! Формат + (код страны) (код оператора) (номер)")
                    return false
                }
            } else {
                setErrorLabelConfiguration(underlineLabel: phoneUnderlineLabel, errorLabel: phoneErrorLabel, constrain: phoneErrorLabelHeight, text: "Ошибка! Поле пустое. Заполните поле обязательно!")
                return false
            }
        } else {
            return false
        }
    }
    
    func checkEmailText() -> Bool {
        if let email = emailText.text {
            if !email.isEmpty {
                if email.contains("@"), email.count > 3 {
                    emailEx = email
                    return true
                } else {
                    setErrorLabelConfiguration(underlineLabel: emailUnderlineLabel, errorLabel: emailErrorLabel, constrain: emailErrorLabelHeight, text: "Ошибка! Введенный адрес не являеться валидным email адресом!")
                    return false
                }
            } else {
                setErrorLabelConfiguration(underlineLabel: emailUnderlineLabel, errorLabel: emailErrorLabel, constrain: emailErrorLabelHeight, text: "Ошибка! Поле пустое. Заполните поле обязательно!")
                return false
            }
        } else {
            return false
        }
    }
    
    func checkCityText() -> Bool {
        if let city = cityText.text {
            if city.isEmpty || city.count > 2 {
                cityEx = city
                return true
            } else {
                setErrorLabelConfiguration(underlineLabel: cityUnderlineLabel, errorLabel: cityErrorLabel, constrain: cityErrorLabelHeight, text: "Ошибка! Непправильное название города! Поле должно содержать от 2 символов!")
                return false
            }
        } else {
            return false
        }
    }
    
    func setErrorLabelConfiguration(underlineLabel : UILabel, errorLabel : UILabel, constrain : NSLayoutConstraint, text : String) {
        underlineLabel.backgroundColor = .red
        errorLabel.layer.borderColor = UIColor.red.cgColor
        errorLabel.textColor = .red
        errorLabel.text = text
        constrain.priority = UILayoutPriority(rawValue: 900)
    }
    
    func resetErrorLabelConfiguration(underlineLabel : UILabel, errorLabel : UILabel, constrain : NSLayoutConstraint) {
        underlineLabel.backgroundColor = .clear
        errorLabel.text = nil
        constrain.priority = UILayoutPriority(rawValue: 600)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case nameText: resetErrorLabelConfiguration(underlineLabel: nameUnderlineLabel, errorLabel: nameErrorLabel, constrain: nameErrorLabelHeight)
        case surnameText: resetErrorLabelConfiguration(underlineLabel: surnameUnderlineLabel, errorLabel: surnameErrorLabel, constrain: surnameErrorLabelHeight)
        case ageText: resetErrorLabelConfiguration(underlineLabel: ageUnderlineLabel, errorLabel: ageErrorLabel, constrain: ageErrorLabelHeight)
        case phoneText: resetErrorLabelConfiguration(underlineLabel: phoneUnderlineLabel, errorLabel: phoneErrorLabel, constrain: phoneErrorLabelHeight)
        case emailText: resetErrorLabelConfiguration(underlineLabel: emailUnderlineLabel, errorLabel: emailErrorLabel, constrain: emailErrorLabelHeight)
        case cityText: resetErrorLabelConfiguration(underlineLabel: cityUnderlineLabel, errorLabel: cityErrorLabel, constrain: cityErrorLabelHeight)
        default:
            break
        }
        
    }
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if checkNameSurnameText(), checkAgeText(), checkEmailText(), checkPhoneText(), checkCityText() {
            let vc = storyboard?.instantiateViewController(withIdentifier: "InfoViewController") as! InfoViewController
            vc.name = nameEx
            vc.surname = surnameEx
            vc.age = ageEx
            vc.phone = phoneEx
            vc.email = emailEx
            vc.city = cityEx
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

