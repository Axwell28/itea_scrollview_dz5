//
//  InfoViewController.swift
//  ITEA_ScrollView
//
//  Created by Alex Marfutin on 7/8/19.
//  Copyright © 2019 ITEA. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var surnameLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var cityLabel: UILabel!
    
    var name = ""
    var surname = ""
    var age = ""
    var phone = ""
    var email = ""
    var city = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = name
        surnameLabel.text = surname
        ageLabel.text = age
        emailLabel.text = email
        phoneLabel.text = phone
        cityLabel.text = city
       
    }
}
