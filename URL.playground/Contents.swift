import UIKit

var str = "Hello, playground"

let url = URL(string: "http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&rvsection=0&titles=pizza&format=json")!


class MainRevision {
    
    var main : String
    var revision : String
    
    init(main : String, revision : String) {
        self.main = main
        self.revision = revision
    }
}

class NormalizePage {
    
    var from : String
    var to: String
    
    init(from : String, to : String) {
        self.from = from
        self.to = to
    }
}
class Numbered {
    
    var pageid : Int
    var ns : Int
    var title : String
    var revisions : [Revision]
    
    init(pageid : Int, ns : Int, title : String, revisions : [Revision]) {
        self.pageid = pageid
        self.ns = ns
        self.title = title
        self.revisions = revisions
    }
}
class Revision {
    
    var contentformat : String
    var contentmodel : String
    var zvizda : String
    
    init(contentformat : String, contentmodel : String, zvizda : String) {
        self.contentformat = contentformat
        self.contentmodel = contentmodel
        self.zvizda = zvizda
    }
}
/*
 
 http://en.wikipedia.org - HOST URL
 /w/api.php - API URL
 action=query - PARAMETR "action" with value "query"
 */

var revisionClass = Revision(contentformat: "", contentmodel: "", zvizda: "")
var numberedClass = Numbered(pageid: 0, ns: 0, title: "", revisions: [])
var normalizedClass = NormalizePage(from: "", to: "")
var mainrevisionClass = MainRevision(main: "", revision: "")
var request = URLRequest(url: url)
request.httpMethod = "GET"



let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
    guard let data = data else {
        return
    }
    //print(String(data: data, encoding: .utf8)!)
    
    do {
        // make sure this JSON is in the format we expect
        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any?] {
            if let batchcomplete = json["batchcomplete"] as? String {
                debugPrint("batchcomplete : \(batchcomplete)")
            }
            if let warnings = json["warnings"] as? [String: Any?] {
                    if let main = warnings["main"] as? [String: Any?] {
                        if let messgae = main["*"] as? String {
                            mainrevisionClass.main = messgae
                            debugPrint("* : \(messgae)")
                        }
                    }
                    if let main = warnings["revisions"] as? [String: Any?] {
                        if let messgae = main["*"] as? String {
                            mainrevisionClass.revision = messgae
                            debugPrint("* : \(messgae)")
                        }
                    }
            }
            if let query = json["query"] as? [String: Any?] {
                if let normalized = query["normalized"] as? [[String : String]] {
                    if let from = normalized[0]["from"] as? String {
                        normalizedClass.from = from
                        debugPrint("from : \(from)")
                    }
                    if let to = normalized[0]["to"] as? String {
                        normalizedClass.to = to
                        debugPrint("to : \(to)")
                    }
                }
                if let pages = query["pages"] as? [String : Any?] {
                    if let number = pages["24768"] as? [String : Any?] {
                        if let pageid = number["pageid"] as? Int {
                            numberedClass.pageid = pageid
                            debugPrint("pageid : \(pageid)")
                        }
                        if let ns = number["ns"] as? Int {
                            numberedClass.ns = ns
                            debugPrint("ns : \(ns)")
                        }
                        if let title = number["title"] as? String {
                            numberedClass.title = title
                            debugPrint("title : \(title)")
                        }
                        if let revisions = number["revisions"] as? [[String : String]] {
                            
                            if let contentformat = revisions[0]["contentformat"] as? String {
                                revisionClass.contentformat = contentformat
                                debugPrint("contentformat : \(contentformat)")
                            }
                            if let contentmodel = revisions[0]["contentmodel"] as? String {
                                revisionClass.contentmodel = contentmodel
                                debugPrint("contentmodel : \(contentmodel)")
                            }
                            if let zvizda = revisions[0]["*"] as? String {
                                revisionClass.zvizda = zvizda
                                debugPrint("* : \(zvizda)")
                            }
                            numberedClass.revisions = [revisionClass]
                        }
                    }
                }
            }
        }
    } catch let error as NSError {
        print("Failed to load: \(error.localizedDescription)")
    }
}

task.resume()
